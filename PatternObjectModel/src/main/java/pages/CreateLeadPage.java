package pages;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods {

	public CreateLeadPage() {		
	PageFactory.initElements(driver, this);
	}
	@FindBy(id="createLeadForm_companyName")
	WebElement eleCname;
	public CreateLeadPage typeCompanyName(String data) {
	type(eleCname, data);
	return this;
	}
	@FindBy(id="createLeadForm_firstName")
	WebElement eleFname;
	public CreateLeadPage typeFirstName(String data) {
	type(eleFname, data);
	return this;
	}
	@FindBy(id="createLeadForm_lastName")
	WebElement eleLname;
	public CreateLeadPage typeLastName(String data) {
	type(eleLname, data);
	return this;
	}
	@FindBy(id="createLeadForm_primaryEmail")
	WebElement eleEmail;
	public CreateLeadPage typeEmail(String data) {
	type(eleEmail, data);
	return this;
	}
	@FindBy(id = "createLeadForm_companyName")
	private WebElement elecompanyname;
	public CreateLeadPage typeCompanyname(String data) {
		type(elecompanyname, data);
		return this;
	}
		
		
		@FindBy(id = "createLeadForm_firstName")
		private WebElement elefname;
		public CreateLeadPage typefname(String data) {
			type(elefname, data);
			return this;
		}
	
		
		@FindBy(id = "createLeadForm_lastName")
		private WebElement elelname;
		public CreateLeadPage typelname(String data) {
			type(elelname, data);
			return this;
		}
		
		@FindBy(id = "createLeadForm_dataSourceId")
		private WebElement elesource;
		public CreateLeadPage sourcedropdown(int data) {
			selectDropDownUsingIndex(elesource, data);
			return this;
		}
		
		@FindBy(id = "createLeadForm_marketingCampaignId")
		private WebElement eleCampaign;
		public CreateLeadPage dropdown(int data) {
			selectDropDownUsingIndex(eleCampaign, data);
			return this;
		}
		
		@FindBy(id = "createLeadForm_personalTitle")
		private WebElement eleper;
		public CreateLeadPage typeper(String data) {
			type(eleper, data);
			return this;
		}
		
		@FindBy(id = "createLeadForm_generalProfTitle")
		private WebElement eleprof;
		public CreateLeadPage typeprof(String data) {
			type(eleprof, data);
			return this;
		}
		@FindBy(id = "createLeadForm_annualRevenue")
		private WebElement eleannual;
		public CreateLeadPage typeannual(String data) {
			type(eleannual, data);
			return this;
		}
		
		@FindBy(id = "createLeadForm_industryEnumId")
		private WebElement eleind;
		public CreateLeadPage industry(String data) {
			selectDropDownUsingText(eleind, data);
			return this;
		}
		
		@FindBy(id = "createLeadForm_ownershipEnumId")
		private WebElement eleowner;
		public CreateLeadPage owner(String data) {
			selectDropDownUsingText(eleowner, data);
			return this;
		}
		
		
		
		@FindBy(id = "createLeadForm_sicCode")
		private WebElement elesic;
		public CreateLeadPage typesic(String data) {
			type(elesic, data);
			return this;
		}
		@FindBy(id = "createLeadForm_description")
		private WebElement eledescription;
		public CreateLeadPage typedes(String data) {
			type(eledescription, data);
			return this;
		}
		
		@FindBy(id = "createLeadForm_importantNote")
		private WebElement elenote;
		public CreateLeadPage typenote(String data) {
			type(elenote, data);
			return this;
		}
		
		@FindBy(id = "createLeadForm_lastNameLocal")
		private WebElement elelocal;
		public CreateLeadPage typelocal(String data) {
			type(elelocal, data);
			return this;
		}
		@FindBy(id = "createLeadForm_departmentName")
		private WebElement eledep;
		public CreateLeadPage typedepartment(String data) {
			type(eledep, data);
			return this;
		}
		@FindBy(id = "createLeadForm_numberEmployees")
		private WebElement eleemployee;
		public CreateLeadPage typeemp(String data) {
			type(eleemployee, data);
			return this;
		}
		@FindBy(id = "createLeadForm_tickerSymbol")
		private WebElement eleticket;
		public CreateLeadPage typeticket(String data) {
			type(eleticket, data);
			return this;
		}
		@FindBy(id = "createLeadForm_primaryPhoneCountryCode")
		private WebElement elecountry;
		public CreateLeadPage typecoountry(String data) {
			type(elecountry, data);
			return this;
		}
		@FindBy(id = "createLeadForm_primaryPhoneAreaCode")
		private WebElement elearea;
		public CreateLeadPage typearea(String data) {
			type(elearea, data);
			return this;
		}
		@FindBy(id = "createLeadForm_primaryPhoneExtension")
		private WebElement eleext;
		public CreateLeadPage typeext(String data) {
			type(eleext, data);
			return this;
		}
		@FindBy(id = "createLeadForm_primaryEmail")
		private WebElement eleemail;
		public CreateLeadPage typeemail(String data) {
			type(eleemail, data);
			return this;
		}
		@FindBy(id = "createLeadForm_primaryPhoneNumber")
		private WebElement elenum;
		public CreateLeadPage typenum(String data) {
			type(elenum, data);
			return this;
		}
		@FindBy(id = "createLeadForm_primaryPhoneAskForName")
		private WebElement eleask;
		public CreateLeadPage typeask(String data) {
			type(eleask, data);
			return this;
		}
		
		@FindBy(id = "createLeadForm_primaryWebUrl")
		private WebElement eleurl;
		public CreateLeadPage typeurl(String data) {
			type(eleurl, data);
			return this;
		}
	@FindBy(className="smallSubmit")
	WebElement eleCreateButton;
	public ViewLeadPage clickCreateButton() {
	click(eleCreateButton);
	return new ViewLeadPage();
	}
	
}







